# Vault

Dans cette aventure, vous allez avoir besoin de renforcer la sécurité votre `sith`. Pour cela, une des options pourra être l'utilisation du gourdin [BanzaiCloud Bank Vault](https://github.com/banzaicloud/bank-vaults) qui vous permettra de distribuer de façon sécurisé vos différents secrets.

## Se connecter au vault

```sh
kubectl -n vault-operator port-forward vault-0 8200 &
PID=$!

export VAULT_ADDR=https://127.0.0.1:8200
kubectl -n vault-operator get secret vault-tls -o jsonpath="{.data.ca\.crt}" | base64 --decode > $PWD/vault-ca.crt
export VAULT_CACERT=$PWD/vault-ca.crt

export VAULT_TOKEN=$(kubectl -n vault-operator get secrets vault-unseal-keys -o jsonpath={.data.vault-root} | base64 --decode)
...
kill $PID
```

## Ajout d'un secret dans vault

```sh
vault kv put secret/my-secret content=my-secure-secret
```

## Injecter un secret vault dans un secret kubernetes

L'injection de secret dans un manifest de pod par exemple ne nécessite pas de l'encoder en base64.

```sh
SECRET=$(echo -n '${vault:secret/data/my-secret#content}' | base64 -w0)
cat > secret.yml <<EOF
---
apiVersion: v1
kind: Secret
metadata:
  name: my-secret
  annotations:
    vault.security.banzaicloud.io/vault-addr: "https://vault.vault-operator:8200"
    vault.security.banzaicloud.io/vault-tls-secret: "vault-tls"
    # Permet de notifier le webhook d'aller inspecter le contenu du secret.
    vault.security.banzaicloud.io/inline-mutation: 'true'
data:
  secret: ${SECRET}
EOF
kubectl apply -f secret.yml
```
